class_name BulletContainer, "res://assets/bullets/bullets_container_img.png"
extends Node

func _ready() -> void:
	add_to_group("bullets_container")
